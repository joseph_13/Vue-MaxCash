import Vue from 'vue'
import Router from 'vue-router'
import form_1 from '@/components/form_1'
import form_1_5 from '@/components/form_1_5'
import form_2_5 from '@/components/form_2_5'
import form_2 from '@/components/form_2'
import form_3 from '@/components/form_3'
import form_4 from '@/components/form_4'
import form_5 from '@/components/form_5'
import form_6 from '@/components/form_6'
import form_7 from '@/components/form_7'
import form_8 from '@/components/form_8'
import form_9 from '@/components/form_9'
import form_10 from '@/components/form_10'
import form_11 from '@/components/form_11'

import Loan_Mart_1 from '@/components/Loan_Mart_1'
import Loan_Mart_2 from '@/components/Loan_Mart_2'
import Loan_Mart_3 from '@/components/Loan_Mart_3'
import Loan_Mart_4 from '@/components/Loan_Mart_4'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'form_1',
      component: form_1
    },
    {
      path: '/form_1_5',
      name: 'form_1_5',
      component: form_1_5
    },
    {
      path: '/form_2',
      name: 'form_2',
      component: form_2
    },
    {
      path: '/form_2_5',
      name: 'form_2_5',
      component: form_2_5
    },
    {
      path: '/form_3',
      name: 'form_3',
      component: form_3
    },
    {
      path: '/form_4',
      name: 'form_4',
      component: form_4
    },
    {
      path: '/form_5',
      name: 'form_5',
      component: form_5
    },
    {
      path: '/form_6',
      name: 'form_6',
      component: form_6
    },        
    {
      path: '/form_7',
      name: 'form_7',
      component: form_7
    },
    {
      path: '/form_8',
      name: 'form_8',
      component: form_8
    },
    {
      path: '/form_9',
      name: 'form_9',
      component: form_9
    },   
    {
      path: '/form_10',
      name: 'form_10',
      component: form_10
    },             
    {
      path: '/form_11',
      name: 'form_11',
      component: form_11
    },
    {
      path: '/Loan_Mart_1',
      name: 'Loan_Mart_1',
      component: Loan_Mart_1
    },
    {
      path: '/Loan_Mart_2',
      name: 'Loan_Mart_2',
      component: Loan_Mart_2
    },
    {
      path: '/Loan_Mart_3',
      name: 'Loan_Mart_3',
      component: Loan_Mart_3
    },
    {
      path: '/Loan_Mart_4',
      name: 'Loan_Mart_4',
      component: Loan_Mart_4
    },    
  ]
})
