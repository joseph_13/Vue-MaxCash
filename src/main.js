// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VeeValidate, {Validator} from 'vee-validate'
import Multiselect from 'vue-multiselect'

Vue.config.productionTip = false

//Configurations for Vee-Validate
const config = {
		// aria: true,
		// classNames: {},
		// classes: false,
		// delay: 0,
		// dictionary,
		// errorBagName: 'errors',			// Change if property conflicts
		// events: 'input|blur',
		fieldsBagName: 'vvfields',			// Change fieldbag name to avoid conflict with vue-instance variables
		// i18n: null,						// The vue-i18n plugin instance
		// i18nRootKey: 'validations',		// The nested key under which the validation messsages will be located
		// inject: true,
		// locale: 'en',
		// strict: true,
		// validity: false,
};

// Vee-Validate custom error fields
const dictionary = {
	custom: {
		firstName: {
			required: 'First Name is not specified.',
			alpha_spaces: 'First Name can only consist of letters and spaces.',
		},
		lastName: {
			required: 'Last Name is not specified.',
			alpha_spaces: 'Last Name can only consist of letters and spaces.',
		},
		email: {
			required: 'Email Address is not specified.',
			email: 'Email address is invalid.',
		},
		states: {
			required: 'Please select a state.',
		},
		automobile: {
			required: 'Please select an option.',
		},

		car_year: {
			required: 'Vehicle Year is not specified.',
		},
		car_make: {
			required: 'Vehicle Make is not specified.',
		},
		car_model: {
			required: 'Vehicle Model is not specified.',
		},
		car_style: {
			required: 'Vehicle Style is not specified.',
		},
		car_mileage: {
			required: 'Vehicle Mileage is not specified.',
			integer: 'Vehicle Mileage can only consist of numbers.',
			min_value: 'Vehicle Mileage must be 0 or more.',
			max_value: 'Vehicle mileage must be under 500,000 miles.',
			max: 'Please enter less than 7 characters.',
		},
		mobile_number: {
			required: 'Phone Number is not specified.',
			regex: 'Phone Number is invalid.',
			min: 'Phone Number must consist of at least 10 characters.',
			max: 'Phone Number must consist of less than 11 characters.',
		},
		text_active_military: {
			required: 'You must agree to our terms & conditions.',
		},

		street_address: {
			required: 'Street Address is not specified.',
			regex: 'Street Address can only consist of letters, numbers, and spaces.',
		},
		city: {
			required: 'City is not specified.',
			alpha_spaces: 'City can only consist of letters and spaces.',
		},
		zip_code: {
			required: 'Zip Code is not specified.',
			integer: 'Zip Code can only consist of numbers.',
			min_value: 'Zip Code is incorrect.',
			max_value: 'Zip Code is incorrect.',
			min: 'Zip Code is incorrect.',
			max: 'Zip Code is incorrect.',
		},
		ownership: {
			required: 'Ownership is not specified.',
		},
		terms: {
			required: 'You must agree to our terms & conditions.',
		},
		ownership: {
			required: 'Ownership is not specified.',
		},					
	},
};

//Vue Multiselect Plugin
Vue.component('multiselect', Multiselect)

// Apply the dictionary above to english.
Validator.localize('en', dictionary)

Vue.use(VeeValidate, config)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
    template: '<App/>'
})
